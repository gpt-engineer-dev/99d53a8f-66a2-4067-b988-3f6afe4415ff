document.getElementById('task-form').addEventListener('submit', function(e) {
  e.preventDefault();

  // Get the task input value
  const task = document.getElementById('task-input').value;

  // Create a new list item
  const li = document.createElement('li');
  li.className = 'border-b border-gray-200 py-2 flex justify-between items-center';

  // Add the task text
  li.innerHTML = `<span>${task}</span> <i class="fas fa-trash-alt text-red-500 cursor-pointer" onclick="deleteTask(event)"></i>`;

  // Append the list item to the task list
  document.getElementById('task-list').appendChild(li);

  // Clear the task input value
  document.getElementById('task-input').value = '';
});

function deleteTask(e) {
  if (confirm('Are you sure you want to delete this task?')) {
    e.target.parentElement.remove();
  }
}
